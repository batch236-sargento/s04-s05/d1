package com.zuitt.activity;

public class Main {

    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();

        contact1.setName("John Doe");
        contact1.setContactNumber("+639152468596");
        contact1.setAddress("Quezon City");

        contact2.setName("Jane Doe");
        contact2.setContactNumber("+639162148573");
        contact2.setAddress("Caloocan City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if(phonebook.getContacts().size() == 0)
        {
            System.out.println("Phonebook is Empty. Add a contact");
        }else {
            phonebook.getContacts().forEach(contact -> {
                System.out.println("---------------");
                System.out.println(contact.getName());
                System.out.println("---------------");

                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());

                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("my home in " + contact.getAddress());
            });
        }
    }
}

